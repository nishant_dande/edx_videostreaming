package com.clarice.offlinestreaming.model;

public class Video {

	public String ext = "";
	public String type = "";
	public String url = "";

	public Video(String ext, String type, String url) {
		this.ext = ext;
		this.type = type;
		this.url = url;
	}

}
