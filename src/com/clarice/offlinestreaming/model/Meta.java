package com.clarice.offlinestreaming.model;

public class Meta {

	public String num;
	public String type;
	public String ext;

	public Meta(String num, String ext, String type) {
		this.num = num;
		this.ext = ext;
		this.type = type;
	}
}
