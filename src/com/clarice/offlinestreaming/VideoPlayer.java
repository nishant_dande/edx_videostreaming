package com.clarice.offlinestreaming;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

import com.clarice.offlinestreaming.network.IDownloadListener;
import com.clarice.offlinestreaming.network.IYoutubeDataListener;
import com.clarice.offlinestreaming.network.impl.DownloaderStream;
import com.clarice.offlinestreaming.network.impl.YoutubeUrlTask;

public class VideoPlayer extends FragmentActivity implements OnClickListener {

	private static final String YOUTUBE_URL="http://www.youtube.com/watch?v";
	
	private Spinner mSpinnerVideoList;
	private VideoView mVideoViewSimpleVideo;
	private Button mButtonPlayVideo;
	private ProgressBar mProgressBarVideoPlay;
	private Context mContext;
	private Handler handler=new Handler();
	
	private int mTrackStatus=0;
	private boolean mSyncWithSDCardVideo=false;
	private boolean mIfEndOfVideo=false;
	private int position;
	private String mGlobalUri;
	private Handler mHandlerForBufferUpdate=new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=VideoPlayer.this;
		
		setContentView(R.layout.video_fragment);
		
		initializeComponent();
		componentControl();
		
		final String[] strings={
				"http://www.boisestatefootball.com/sites/default/files/videos/original/01%20-%20coach%20pete%20bio_4.mp4",
				"http://www.youtube.com/watch?v=fzG4BcaK064",
				"http://haignet.co.uk/html5-video-element-test.mp4",
				"http://www.youtube.com/watch?v=40DykbPa4Lc",
				"http://www.youtube.com/watch?v=uwn-Tfc4P2Q"};
		
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, strings);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerVideoList.setAdapter(adapter);
		SharedPreferences preferences=getPreferences(mContext);
		
		boolean shouldPlay=preferences.getBoolean("shouldPlay", false);
		Log.e("TAG", shouldPlay+"");
		if (shouldPlay) {
			String uri=preferences.getString("uri", "");
			Log.e("TAG", uri+"");
			if (uri.contains("http://")|| uri.contains("https://")) {
				runTheVideo(uri);
			}else{
				runVideoFromSDCard(uri);				
			}
			mVideoViewSimpleVideo.seekTo(preferences.getInt("position", 0));
			mVideoViewSimpleVideo.start();
		}
		
		mVideoViewSimpleVideo.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mIfEndOfVideo=true;
				SharedPreferences preferences=getPreferences(mContext);
				Editor editor=preferences.edit();
				editor.putBoolean("shouldPlay", false);
				editor.commit();
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (mVideoViewSimpleVideo!=null) {
			mVideoViewSimpleVideo.seekTo(position);
			mVideoViewSimpleVideo.start();			
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mVideoViewSimpleVideo!=null) {
			position=mVideoViewSimpleVideo.getCurrentPosition();
			mVideoViewSimpleVideo.pause();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (!mIfEndOfVideo) {			
			SharedPreferences preferences=getPreferences(mContext);
			Editor editor=preferences.edit();
			editor.putBoolean("shouldPlay", true);
			editor.putString("uri", mGlobalUri);
			editor.putInt("position", mVideoViewSimpleVideo.getCurrentPosition());
			editor.commit();
		}
	}
	
	private SharedPreferences getPreferences(Context context) {
	    return getSharedPreferences(VideoPlayer.class.getSimpleName(),Context.MODE_PRIVATE);
	}
	
	private void componentControl() {
		mButtonPlayVideo.setOnClickListener(this);
	}

	private void initializeComponent() {
		mSpinnerVideoList=(Spinner)findViewById(R.id.listOfVideoUrl);
		mVideoViewSimpleVideo=(VideoView)findViewById(R.id.VideoView);
		mButtonPlayVideo=(Button)findViewById(R.id.play);
		mProgressBarVideoPlay=(ProgressBar)findViewById(R.id.videoStatusProgressBar);
	}
	
	private void runTheVideo(final String url)
	{
		mGlobalUri=url;
		try {
            MediaController mediacontroller = new MediaController(mContext);
            mediacontroller.setAnchorView(mVideoViewSimpleVideo);
            mediacontroller.setMediaPlayer(mVideoViewSimpleVideo);
            Uri video = Uri.parse(url);
            mVideoViewSimpleVideo.setMediaController(mediacontroller);
            mVideoViewSimpleVideo.setVideoURI(video);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mVideoViewSimpleVideo.requestFocus();
        
        mVideoViewSimpleVideo.setOnErrorListener(new OnErrorListener()
        {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) 
			{
				if (!checkInternetIsPresent())
				{
					Toast.makeText(mContext, "please check internet connection", Toast.LENGTH_LONG).show();
				}
				return true;
			}
		});
        mVideoViewSimpleVideo.setOnPreparedListener(new OnPreparedListener()
        {
            public void onPrepared(MediaPlayer mp) 
            {
            	mp.setOnBufferingUpdateListener(new OnBufferingUpdateListener() 
            	{
					@Override
					public void onBufferingUpdate(MediaPlayer mp, int percent) 
					{
					}
				});
            	mProgressBarVideoPlay.setVisibility(View.GONE);
                mVideoViewSimpleVideo.start();
            }
        });
	}
	
	private boolean checkInternetIsPresent(){
		ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null &&activeNetwork.isConnectedOrConnecting();
	}
	
	
	private void runVideoFromSDCard(final String mUri){
		mGlobalUri=mUri;
		try {
            MediaController mediacontroller = new MediaController(mContext);
            mediacontroller.setAnchorView(mVideoViewSimpleVideo);
            mediacontroller.setMediaPlayer(mVideoViewSimpleVideo);
            mVideoViewSimpleVideo.setMediaController(mediacontroller);
            mVideoViewSimpleVideo.setVideoPath(mGlobalUri);
            if (!mSyncWithSDCardVideo) {
            	mVideoViewSimpleVideo.seekTo(mTrackStatus);		
            }
		} catch (Exception e) {
            e.printStackTrace();
        }
        mVideoViewSimpleVideo.requestFocus();
        
        mVideoViewSimpleVideo.setOnErrorListener(new OnErrorListener()
        {
			@Override
			public boolean onError(final MediaPlayer mp, int what, int extra)
			{
				switch (extra) {
				case MediaPlayer.MEDIA_ERROR_IO:
				{
					if (!checkInternetIsPresent()) 
					{
						Toast.makeText(mContext, "please check internet connection", Toast.LENGTH_LONG).show();
						mVideoViewSimpleVideo.stopPlayback();
						return true;
					}
					mTrackStatus=mp.getCurrentPosition();
					mHandlerForBufferUpdate.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							runVideoFromSDCard(mUri);
						}
					}, 2000);
				}
					break;
					
				case MediaPlayer.MEDIA_ERROR_MALFORMED:
				{
					Log.d("TAG", "MEDIA_ERROR_MALFORMED");
				}break;
				
				case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
				{
					Log.d("TAG", "MEDIA_ERROR_TIMED_OUT");
				}break;
				
				case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
				{
					Log.d("TAG", "MEDIA_ERROR_UNSUPPORTED");
				}break;
				
				case MediaPlayer.MEDIA_INFO_BUFFERING_END:
				{
					Log.d("TAG", "MEDIA_INFO_BUFFERING_END");
				}break;
				
				case MediaPlayer.MEDIA_INFO_BUFFERING_START:
				{
					Log.d("TAG", "MEDIA_INFO_BUFFERING_START");
				}break;

				default:
					break;
				}
				return true;
			}
			
		});
        
        mVideoViewSimpleVideo.setOnPreparedListener(new OnPreparedListener()
        {
            public void onPrepared(MediaPlayer mp) 
            {
            	mProgressBarVideoPlay.setVisibility(View.GONE);
                mVideoViewSimpleVideo.start();
            }
        });
	}
	
	@Override
	public void onClick(View view) 
	{
		String videoURL=mSpinnerVideoList.getSelectedItem().toString();
		boolean isYoutubeVideo=false;
		if (videoURL.contains(YOUTUBE_URL))
		{
			isYoutubeVideo=true;
		}
		
		switch (view.getId()) 
		{
			case R.id.play: 
			{
				mProgressBarVideoPlay.setVisibility(View.VISIBLE);
				if (!isYoutubeVideo) 
				{
					downloadTheVideo(videoURL);			
				} 
				else
				{
					new YoutubeUrlTask(mContext, new IYoutubeDataListener() 
					{
						@Override
						public void onRetrieveDataComplete(boolean b, String mYoutubeVideoURL) 
						{
							if (b) 
							{
								downloadTheVideo(mYoutubeVideoURL);								
							}else{
								Toast.makeText(mContext, "unable to find url please try again", Toast.LENGTH_LONG).show();
								mProgressBarVideoPlay.setVisibility(View.GONE);
							}
						}
					},true).execute(videoURL);
				}
			}
			break;
	
			default:
			break;
		}
	}
	
	

	private void downloadTheVideo(String videoUrl)
	{
		new DownloaderStream(mContext,new IDownloadListener() 
		{
			@Override
			public void onDownload(Context context, boolean b, final String mVideoURI) 
			{
				if (b) 
				{
					try {
						handler.post(new Runnable() {

							@Override
							public void run() {
								runVideoFromSDCard(mVideoURI);								
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					mProgressBarVideoPlay.setVisibility(View.GONE);
				}
			}

			@Override
			public void onPeriodicUpdate(boolean b, String mUpdateUri,int bufferPerentage) 
			{
				int totalDuration=mVideoViewSimpleVideo.getDuration();
				int currrentDuration=mVideoViewSimpleVideo.getCurrentPosition();
				int mediaPercentage=currrentDuration*100/totalDuration;
				Log.d("TAG", "percentage media : "+mediaPercentage+" percentage : "+bufferPerentage);
				int setBufferPercentage=bufferPerentage-2;
				
				if((mediaPercentage>setBufferPercentage) && (mediaPercentage<bufferPerentage)) {
					mVideoViewSimpleVideo.pause();
				}else{
					mVideoViewSimpleVideo.start();
				}
				
				/*if (b) {
					Log.d("TAG", "percentage : "+bufferPerentage);
				}*/
			}

		}).execute(videoUrl);
	}
}
