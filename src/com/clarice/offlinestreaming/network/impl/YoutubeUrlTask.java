package com.clarice.offlinestreaming.network.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clarice.offlinestreaming.R;
import com.clarice.offlinestreaming.model.Meta;
import com.clarice.offlinestreaming.model.Video;
import com.clarice.offlinestreaming.network.IYoutubeDataListener;

public class YoutubeUrlTask extends AsyncTask<String, Void, String> {
	
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mNotificationCompat;
	private IYoutubeDataListener mYoutubeDataListener;
	private boolean mIsToRunOnly=false;
	private Context mContext;
	private static int callAgainYoutubeUrl=0;
	private String url;
	
	public YoutubeUrlTask(Context mContext,IYoutubeDataListener mYoutubeDataListener) {
		this.mContext=mContext;
		this.mYoutubeDataListener=mYoutubeDataListener;
	}
	
	public YoutubeUrlTask(Context mContext,IYoutubeDataListener mYoutubeDataListener,boolean mIsToRunOnly) {
		this.mContext=mContext;
		this.mYoutubeDataListener=mYoutubeDataListener;
		this.mIsToRunOnly=mIsToRunOnly;
	}
	
	public YoutubeUrlTask(Context mContext) {
		this.mContext=mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (!mIsToRunOnly) {
			mNotificationManager =
					(NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationCompat = new NotificationCompat.Builder(mContext);
			mNotificationCompat.setContentTitle("Preparing to download..")
			.setSmallIcon(R.drawable.ic_action_refresh);
			mNotificationCompat.setProgress(0, 0, true);
			mNotificationManager.notify(0, mNotificationCompat.build());			
		}
	}

	@Override
	protected String doInBackground(String... params) {
		url = params[0];
	
		try {
		ArrayList<Video> videos= getStreamingUrisFromYouTubePage(url);
		if (videos != null && !videos.isEmpty()) {
            String retVidUrl = null;
            for (Video video : videos) {
                if (video.ext.toLowerCase().contains("mp4")
                        && video.type.toLowerCase().contains("medium")) {
                    retVidUrl = video.url;
                    break;
                }
            }
            if (retVidUrl == null) {
                for (Video video : videos) {
                    if (video.ext.toLowerCase().contains("3gp")
                            && video.type.toLowerCase().contains(
                                    "medium")) {
                        retVidUrl = video.url;
                        break;

                    }
                }
            }
            if (retVidUrl == null) {

                for (Video video : videos) {
                    if (video.ext.toLowerCase().contains("mp4")
                            && video.type.toLowerCase().contains("low")) {
                        retVidUrl = video.url;
                        break;

                    }
                }
            }
            if (retVidUrl == null) {
                for (Video video : videos) {
                    if (video.ext.toLowerCase().contains("3gp")
                            && video.type.toLowerCase().contains("low")) {
                        retVidUrl = video.url;
                        break;
                    }
                }
            }
            
            return retVidUrl;
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (!mIsToRunOnly) {
			// mNotificationCompat.setContentText("Download complete");
			mNotificationCompat.setProgress(0, 0, false);
			mNotificationCompat.setAutoCancel(true);
			mNotificationManager.notify(0, mNotificationCompat.build());
			mNotificationManager.cancel(0);
		}
		if (result!=null) {			
			mYoutubeDataListener.onRetrieveDataComplete(true, result);
		}else{
			if (callAgainYoutubeUrl<=3) {
				Log.d("TAG", "not found");
				new YoutubeUrlTask(mContext,mYoutubeDataListener,mIsToRunOnly).execute(url);
			}else{
				mYoutubeDataListener.onRetrieveDataComplete(false, "");				
			}
		}
	}

	
	public ArrayList<Video> getStreamingUrisFromYouTubePage(String ytUrl)
	        throws IOException {
	    if (ytUrl == null) {
	        return null;
	    }
	    Log.w("TAG", ytUrl);

	    // Remove any query params in query string after the watch?v=<vid> in
	    // e.g.
	    // http://www.youtube.com/watch?v=0RUPACpf8Vs&feature=youtube_gdata_player
	    int andIdx = ytUrl.indexOf('&');
	    if (andIdx >= 0) {
	        ytUrl = ytUrl.substring(0, andIdx);
	    }

	    // Get the HTML response
	    String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0.1)";
	    HttpClient client = new DefaultHttpClient();
	    client.getParams().setParameter(CoreProtocolPNames.USER_AGENT,userAgent);
	    HttpGet request = new HttpGet(ytUrl);
	    HttpResponse response = client.execute(request);
	    String html = "";
	    InputStream in = response.getEntity().getContent();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	    StringBuilder str = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        str.append(line.replace("\\u0026", "&"));
	    }
	    in.close();
	    html = str.toString();

	    Log.d("TAG", html);
	    // Parse the HTML response and extract the streaming URIs
	    if (html.contains("verify-age-thumb")) {
//	        Log.w("YouTube is asking for age verification. We can't handle that sorry.");
	        return null;
	    }

	    if (html.contains("das_captcha")) {
//	        Log.w("Captcha found, please try with different IP address.");
	        return null;
	    }

	    Pattern p = Pattern.compile("stream_map\": \"(.*?)?\"");
	    // Pattern p = Pattern.compile("/stream_map=(.[^&]*?)\"/");
	    Matcher m = p.matcher(html);
	    List<String> matches = new ArrayList<String>();
	    while (m.find()) {
	        matches.add(m.group());
	    }

	    if (matches.size() != 1) {
//	        Log.w("Found zero or too many stream maps.");
	        return null;
	    }

	    String urls[] = matches.get(0).split(",");
	    HashMap<String, String> foundArray = new HashMap<String, String>();
	    for (String ppUrl : urls) {
	        String url = URLDecoder.decode(ppUrl, "UTF-8");

	        Pattern p1 = Pattern.compile("itag=([0-9]+?)[&]");
	        Matcher m1 = p1.matcher(url);
	        String itag = null;
	        if (m1.find()) {
	            itag = m1.group(1);
	        }

	        Pattern p2 = Pattern.compile("sig=(.*?)[&]");
	        Matcher m2 = p2.matcher(url);
	        String sig = null;
	        if (m2.find()) {
	            sig = m2.group(1);
	        }

	        Pattern p3 = Pattern.compile("url=(.*?)[&]");
	        Matcher m3 = p3.matcher(ppUrl);
	        String um = null;
	        if (m3.find()) {
	            um = m3.group(1);
	        }

	        if (itag != null && sig != null && um != null) {
	            foundArray.put(itag, URLDecoder.decode(um, "UTF-8") + "&"
	                    + "signature=" + sig);
	        }
	    }

	    if (foundArray.size() == 0) {
//	        Log.w("Couldn't find any URLs and corresponding signatures");
	        return null;
	    }

	    HashMap<String, Meta> typeMap = new HashMap<String, Meta>();
	    typeMap.put("13", new Meta("13", "3GP", "Low Quality - 176x144"));
	    typeMap.put("17", new Meta("17", "3GP", "Medium Quality - 176x144"));
	    typeMap.put("36", new Meta("36", "3GP", "High Quality - 320x240"));
	    typeMap.put("5", new Meta("5", "FLV", "Low Quality - 400x226"));
	    typeMap.put("6", new Meta("6", "FLV", "Medium Quality - 640x360"));
	    typeMap.put("34", new Meta("34", "FLV", "Medium Quality - 640x360"));
	    typeMap.put("35", new Meta("35", "FLV", "High Quality - 854x480"));
	    typeMap.put("43", new Meta("43", "WEBM", "Low Quality - 640x360"));
	    typeMap.put("44", new Meta("44", "WEBM", "Medium Quality - 854x480"));
	    typeMap.put("45", new Meta("45", "WEBM", "High Quality - 1280x720"));
	    typeMap.put("18", new Meta("18", "MP4", "Medium Quality - 480x360"));
	    typeMap.put("22", new Meta("22", "MP4", "High Quality - 1280x720"));
	    typeMap.put("37", new Meta("37", "MP4", "High Quality - 1920x1080"));
	    typeMap.put("33", new Meta("38", "MP4", "High Quality - 4096x230"));

	    ArrayList<Video> videos = new ArrayList<Video>();

	    for (String format : typeMap.keySet()) {
	        Meta meta = typeMap.get(format);

	        if (foundArray.containsKey(format)) {
	            Video newVideo = new Video(meta.ext, meta.type,
	                    foundArray.get(format));
	            videos.add(newVideo);
	            //Log.d("TAG","YouTube Video streaming details: ext:" + newVideo.ext   + ", type:" + newVideo.type + ", url:" + newVideo.url);
	        }
	    }

	    return videos;
	}
}
