package com.clarice.offlinestreaming.network.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clarice.offlinestreaming.R;
import com.clarice.offlinestreaming.network.IDownloadListener;

public class DownloaderStream extends AsyncTask<String, Void, Boolean> {

	private Context mContext;
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mNotificationCompat;
	private Handler handler;
	private IDownloadListener iDownloadListener;
	private String mPath;
	private  String mFileName;
	private boolean flag=false;
	
	public DownloaderStream(Context mContext,IDownloadListener iDownloadListener) {
		this.mContext=mContext;
		this.iDownloadListener=iDownloadListener;
		this.handler=new Handler();
	}
	
	public DownloaderStream(Context mContext) {
		this.mContext=mContext;
		this.handler=new Handler();
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mNotificationManager =
		        (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationCompat = new NotificationCompat.Builder(mContext);
		mNotificationCompat.setContentTitle("Video Download")
		    .setContentText("Download in progress")
		    .setSmallIcon(R.drawable.ic_action_refresh);
		//mNotificationCompat.setProgress(0, 0, true);
		mNotificationManager.notify(0, mNotificationCompat.build());
	}
	
	protected String getName(){
		Format formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss",Locale.ENGLISH);
		return formatter.format(new java.util.Date());
	}
	
	@Override
	protected Boolean doInBackground(String... params) {
		try {
			URL mURL=new URL(params[0]);
			HttpURLConnection c = (HttpURLConnection) mURL.openConnection();
	        c.setRequestMethod("GET");
	        c.setDoOutput(true);
	        c.connect();
	        Log.e("TAG", c.getContentLength()+"");
	        int videoLength=c.getContentLength();
	        mPath = Environment.getExternalStorageDirectory()+ "/video_download/";
	        File file = new File(mPath);
	        file.mkdir();
	       
	        mFileName = getName() + ".mp4";

	        mFileName=mFileName.replace(":", "_");
	        mFileName=mFileName.replace("-", "_");
	        File outputFile = new File(file, mFileName);
	        FileOutputStream fos = new FileOutputStream(outputFile);

	        InputStream is = c.getInputStream();
	        
	        byte[] buffer = new byte[16384];
	        int updateInterval=10;
	        int len1 = 0;
	        long bufferSize=0;
	        try{
	        	while ((len1 = is.read(buffer)) != -1) {
	        		bufferSize+=len1;
	        		fos.write(buffer, 0, len1);

	        		int bufferPercentage=getPercentage(videoLength,(int) bufferSize);
	        		if (bufferPercentage==5) {
	        			iDownloadListener.onDownload(mContext, true,mPath+mFileName);
	        			flag=true;
	        			//return true;
	        		}else if(bufferPercentage==updateInterval){
	        			iDownloadListener.onPeriodicUpdate(true, mPath+mFileName, bufferPercentage);
	        			updateInterval+=5;
	        		}
	        		
	        		updateNotificationBar(videoLength,bufferSize);
	        	}
	        }catch (Exception e) {
	        	e.printStackTrace();
	        }
	        
	        Log.d("TAG", bufferSize+"");
	        fos.close();
	        is.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return false;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}	
	
	private void updateNotificationBar(final int videoLength, final long bufferSize) {
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				mNotificationCompat.setProgress(videoLength, (int)bufferSize, false);
				mNotificationCompat.setContentText(getPercentage(videoLength, (int)bufferSize)+"% downloaded.");
				mNotificationManager.notify(0, mNotificationCompat.build());
			}
		});
	}
	
	private int getPercentage(int totalLength,int bufferLength){
		return (bufferLength*100/totalLength);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		mNotificationCompat.setContentText("Download complete");
		mNotificationCompat.setAutoCancel(true);
		mNotificationManager.notify(0, mNotificationCompat.build());
		mNotificationManager.cancel(0);
		
		/*if (result) {
			//iDownloadListener.onDownload(mContext, true,mFileName);
		}*/
	}
}
