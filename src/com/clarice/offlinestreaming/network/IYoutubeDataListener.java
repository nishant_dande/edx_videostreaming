package com.clarice.offlinestreaming.network;


public interface IYoutubeDataListener {

	void onRetrieveDataComplete(boolean b,String mYoutubeVideoURL);
}
