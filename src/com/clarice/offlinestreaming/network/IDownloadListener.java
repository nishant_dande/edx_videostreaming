package com.clarice.offlinestreaming.network;

import android.content.Context;

public interface IDownloadListener {

	void onDownload(Context context,boolean b,String mVideoURI);
	void onPeriodicUpdate(boolean b,String mUpdateUri,int bufferPerentage);
}
